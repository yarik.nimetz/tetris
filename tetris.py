from tkinter import *
import time
import random

class Tetris:
    def __init__(self):
        self.size_part = 30
        self.wall_size = {'height' : 22,
                          'width' : 12}
        self.used_blocks, self.wall_index, self.blocks_in_use, self.wall_index, \
                          self.element_pause_menu, self.buttons_pause_menu, self.saves_status, self.menu_text, self.wall = [], [], [], [], [], [], [], [], []
        self.cycle, self.order, self.name, self.menu_attempt = 0, 0, 0, 0
        self.tetrominoes = [\
                            #letter_L
                            [[1, 0, 0],\
                            [1, 0, 0],\
                            [1, 1, 0]],\
                            #revers_letter_L
                            [[0, 1, 0],\
                            [0, 1, 0],\
                            [1, 1, 0]],\
                            #block
                            [[1, 1],\
                            [1, 1]],\
                            #letter_I
                            [[0, 1, 0, 0],\
                            [0, 1, 0, 0],\
                            [0, 1, 0, 0],\
                            [0, 1, 0, 0]],\
                            #snake
                            [[0, 1, 1],\
                            [1, 1, 0],\
                            [0, 0, 0]],\
                            #revers_snake
                            [[1, 1, 0],\
                            [0, 1, 1],\
                            [0, 0, 0]],\
                            #tank
                            [[0, 1, 0],\
                            [1, 1, 1],\
                            [0, 0, 0]]]
        self.colour_block = ["Red", "Orange", "Yellow", "Green", "Cyan", "Blue", "Violet"]
        self.condition, self.pause = True, False
        self.start_time = time.time()
        self.tk = Tk()
        self.canvas = Canvas(self.tk, width=self.wall_size['width'] * self.size_part, height=self.wall_size['height'] * self.size_part, bd=0, highlightthickness=0)
        self.canvas.pack()
        self.tk.title('Gra')
        self.tk.resizable(0, 0)
        self.tk.wm_attributes("-topmost", 1)
        self.canvas.bind_all('<KeyPress-Right>', self.move_right) 
        self.canvas.bind_all('<KeyPress-Left>', self.move_left)
        self.canvas.bind_all('<KeyPress-Up>', self.bend)
        self.canvas.bind_all('<Escape>', self.topause) 
        self.canvas.bind_all('<Button-1>', self.mouse_click)
        
    def create_window(self):
        for i in range(self.wall_size['height']):
            self.wall.append([])
            for u in range(self.wall_size['width']):
                if i == 0 or i == self.wall_size['height'] - 1 or u == 0 or u == self.wall_size['width'] -1:
                    self.wall[i].append(1)
                else:
                    self.wall[i].append(0)
                    
        for i in range(len(self.wall)):
            for u in range(len(self.wall[i])):
                if self.wall[i][u]:
                    self.wall_index.append(self.canvas.create_rectangle(self.size_part*u, self.size_part*i,\
                                                                           self.size_part*(u+1), self.size_part*(i+1), fill = "gray"))
            
    def spawn_tetrominoes(self, random_build, x, y, colour):
        for i in range(len(random_build)):
            for u in range(len(random_build[i])):
                if random_build[i][u]:
                    self.blocks_in_use.append(self.canvas.create_rectangle(self.size_part*u, self.size_part*i,\
                                                                           self.size_part*(u+1), self.size_part*(i+1), fill = self.colour_block[colour]))
        for i in self.blocks_in_use:
            self.canvas.move(i,self.size_part * x, self.size_part * y)

    def choose_blocks(self):
        self.position = 5
        self.random_number = random.randrange(0, 7)
        self.random_build = self.tetrominoes[self.random_number]
        self.spawn_tetrominoes(self.random_build, 5, 1, self.random_number) 

    def line_cleare(self):
        coords = 0
        delete = []
        for i in self.used_blocks:
            number = 0
            for u in self.used_blocks:
                if self.canvas.coords(i)[1] == self.canvas.coords(u)[1]:
                    number += 1
                if number == 10:
                    coords = self.canvas.coords(u)[1]
                    for a in self.used_blocks:
                        if coords == self.canvas.coords(a)[1]:
                            delete.append(a)
                            self.canvas.delete(a)
                    for a in delete:
                        self.used_blocks.remove(a)
                    for a in self.used_blocks:
                        if self.canvas.coords(a)[1] <= coords:
                            self.canvas.move(a, 0, self.size_part)
                    self.line_cleare()
                        
    def collision(self, x, y):
        for i in self.blocks_in_use:
            for u in self.wall_index:
                if self.canvas.coords(i)[1] + (self.size_part * y) == self.canvas.coords(u)[1] and\
                   self.canvas.coords(i)[0] + (self.size_part * x) == self.canvas.coords(u)[0]:
                    return True
            for u in self.used_blocks:
                if self.canvas.coords(i)[1] + (self.size_part * y) == self.canvas.coords(u)[1] and\
                   self.canvas.coords(i)[0] + (self.size_part * x) == self.canvas.coords(u)[0]:
                    return True
        return False

    def move_right(self,event):
        if not self.collision(1, 0) and not self.collision(0, 1) and not self.pause:
            for i in self.blocks_in_use:
                self.canvas.move(i,self.size_part,0)
            self.position += 1

    def move_left(self,event):
        if not self.collision(-1, 0) and not self.collision(0, 1) and not self.pause:
            for i in self.blocks_in_use:
                self.canvas.move(i,self.size_part * (-1),0)
            self.position -= 1
                
    def move_down(self):
        if not self.collision(0, 1):
            for i in self.blocks_in_use:
                self.canvas.move(i,0,self.size_part)
        elif self.collision(0, 1) and self.cycle != 1:
            self.cycle = 0
            self.used_blocks = self.used_blocks + self.blocks_in_use
            self.blocks_in_use = []
            self.choose_blocks() 
        else:
            self.condition = False

    def bend(self,event):
        if not self.pause:
            mass1, mass2, cord = self.random_build, [], self.canvas.coords(self.blocks_in_use[0])[1]
            for j in range(len(mass1)):
                mass2.append([])
                for i in range(len(mass1)):
                    mass2[j].append(mass1[-1*(i+1)][j])
            for i in range(len(mass2)):
                for j in range(len(mass2[i])):
                    if mass2[i][j]:
                        for u in self.wall_index:
                            if self.size_part * (self.position + j) == self.canvas.coords(u)[0] and\
                               cord + (self.size_part * i) == self.canvas.coords(u)[1]:
                                return False
                        for u in self.used_blocks:
                            if self.size_part * (self.position + j) == self.canvas.coords(u)[0] and\
                               cord + (self.size_part * i) == self.canvas.coords(u)[1]:
                                return False
                       
            self.random_build = mass2
            for i in self.blocks_in_use:
                self.canvas.delete(i)
            self.blocks_in_use = []
            self.spawn_tetrominoes(self.random_build, self.position, cord//self.size_part, self.random_number)

    def topause(self,event):
        if self.log_status():
            part = self.size_part
            but_text = ['Return', 'Save', 'Load']
            if not self.pause:
                self.element_pause_menu.append(self.canvas.create_rectangle((part*len(self.wall[0]) * (1/4)), (part*len(self.wall) * (1/3)),\
                                (part*len(self.wall[0]) * (3/4)), (part*len(self.wall) * (2/3)), fill = "#8B8378"))
                for i in range(3):
                    self.buttons_pause_menu.append(self.canvas.create_rectangle((part*len(self.wall[0]) * (1/4)) + part, (part*len(self.wall) * (1/3)) + part*(1+2*i), \
                                (part*len(self.wall[0]) * (3/4)) - part, (part*len(self.wall) * (1/3)) + part*2*(i+1), fill = "slateblue"))
                    self.menu_text.append(self.canvas.create_text((part*len(self.wall[0]) * (1/4)) + (part * 3),(part*len(self.wall) * (1/3)) + (part * (3+4*i)/2),\
                                        text = but_text[i],fill='white',font=('Helvetica',int(part * 9/len(self.wall[0])))))
                self.element_pause_menu = self.element_pause_menu + self.buttons_pause_menu + self.menu_text
            else:
                for i in self.element_pause_menu:
                    self.canvas.delete(i)
                self.element_pause_menu, self.buttons_pause_menu, self.menu_text = [], [], []
                self.menu_attempt = 0
            self.pause = not self.pause
        
    def mouse_click(self, event):
        x, y = event.x, event.y
        for i in range(len(self.buttons_pause_menu)):
            if self.canvas.coords(self.buttons_pause_menu[i])[0] <= x and self.canvas.coords(self.buttons_pause_menu[i])[2] >= x\
               and self.canvas.coords(self.buttons_pause_menu[i])[1] <= y and self.canvas.coords(self.buttons_pause_menu[i])[3] >= y:
                if i == 0 and self.menu_attempt == 0:
                    self.topause(None)
                else: 
                    self.pause_event(i) #1 - save, 2 - load
                return True
            
    def log_status(self):
        try:
            file = open('log.txt', "r")
            file.close()
        except:
            log = ""
            for i in range(1, 4):
                log += "<Save%s>Empty</Save%s>" % (str(i), str(i)) + '''
'''
            file = open("log.txt", "w")
            file.write(log)
        file = open('log.txt', "r")
        self.log = file.read()
        file.close()
        self.saves_status = []
        for i in range(1, 4):
            if self.log[self.log.index('<Save' + str(i) + '>') + 7:self.log.index('</Save' + str(i) + '>')] == "Empty":
                self.saves_status.append("Empty")
            else:
                self.saves_status.append("Busy")
        return True
                
    def pause_event(self, choose):
        part = self.size_part
        self.menu_attempt += 1
        if self.menu_attempt == 1:
            self.mode = choose
            for i in self.menu_text:
                self.canvas.delete(i)
            for i in self.element_pause_menu:
                if i in self.menu_text:
                    self.element_pause_menu.remove(i)
            self.menu_text = []
            for i in range(3):
                self.menu_text.append(self.canvas.create_text((part*len(self.wall[0]) * (1/4)) + (part * 3),(part*len(self.wall) * (1/3)) + (part * (3+4*i)/2),\
                                    text = self.saves_status[i],fill='white',font=('Helvetica',int(part * 9/len(self.wall[0])))))
            self.element_pause_menu += self.menu_text
        if self.menu_attempt == 2:
            but = choose
            self.menu_attempt = 0
            if self.mode == 1:
                used_objects = []
                future_log = ''
                for i in self.used_blocks:
                    used_objects.append([int(self.canvas.coords(i)[0]), (self.canvas.coords(i)[1])])
                item = "block" + str(self.random_build) + "/blockposition" + str(self.position) + "/positioncoord" + \
                       str(self.canvas.coords(self.blocks_in_use[0])[1]//self.size_part) + "/coordused_blocks" + str(used_objects) + "/used_blocks"
                for i in range(3):
                    if i == but:
                        future_log += '<Save' + str(i+1) + '>' + item +'</Save' + str(i+1) + '>' + '''
'''
                    else:
                        future_log += self.log[self.log.index('<Save' + str(i+1) + '>'):self.log.index('</Save' + str(i+1) + '>')+8] + '''
'''
                try:
                    file = open('log.txt', "w")
                    file.write(future_log)
                    file.close()
                    self.topause(None)
                except:
                    self.topause(None)
                    return False
            if self.mode == 2:
                try:
                    file = open('log.txt', "r")
                    log = file.read()
                    file.close()
                except:
                    self.topause(None)
                    return False
                logs_info, firs, sec = 0, 0, 0
                for i in range(3):      
                    if i == but and self.log[self.log.index('<Save' + str(i+1) + '>') + 7:self.log.index('</Save' + str(i+1) + '>')] == 'Empty':
                        self.topause(None)
                        return False
                    elif i == but:
                        log_info = self.log[self.log.index('<Save' + str(i+1) + '>') + 7:self.log.index('</Save' + str(i+1) + '>')]
                        self.topause(None)
                for i in self.used_blocks:
                    self.canvas.delete(i)
                for i in self.blocks_in_use:
                    self.canvas.delete(i)
                self.used_blocks, self.blocks_in_use = [], []
                blocks_info = [[],[], int(log_info[log_info.index("position")+8:log_info.index("/position")]),float(log_info[log_info.index("coord")+5:log_info.index("/coord")])]
                order = -1
                for i in log_info[log_info.index("block")+6:log_info.index("/block")-1]:
                    if i == "[":
                        blocks_info[0].append([])
                        order += 1
                    if i == "1" or i == "0":
                        blocks_info[0][order].append(int(i))
                order = -1
                u = -1
                for i in log_info[log_info.index("used_blocks")+12:log_info.index("/used_blocks")-1]:
                    u += 1
                    if i == "[":
                        blocks_info[1].append([])
                        order += 1
                    if not i.isnumeric():
                        firs = sec
                        sec = u
                    if sec - firs > 1:             
                        blocks_info[1][order].append(int(log_info[log_info.index("used_blocks")+13+firs:log_info.index("used_blocks")+12+sec]))
                for i in blocks_info[1]:
                    self.used_blocks.append(self.canvas.create_rectangle(i[0], i[1], i[0] + self.size_part, i[1] + self.size_part, fill = "brown"))
                self.random_build = blocks_info[0]
                self.spawn_tetrominoes(blocks_info[0], blocks_info[2], blocks_info[3], self.random_number)
                        
    def run(self):
        self.create_window()
        self.choose_blocks()
        while self.condition:
            if not self.pause:
                try:
                    self.move_down()
                except:
                    print('game closed')
                    return False
                self.line_cleare()
                self.cycle = self.cycle + 1
            self.tk.update()
            time.sleep(0.25)

game = Tetris()
game.run()
