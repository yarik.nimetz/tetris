# Tetris

Tetris is a simple game released at the end of the last century, but still relevant today. The game has no storyline, 
no special ending, all you have to do is with constantly appearing blocks, completely fill one line to make room for 
the next pieces. Tetris by genre is an endless puzzle, in which the very meaning is the arrangement of figures as compactly 
and closely as possible.

<img title="room" alt="main_room" src="/images/main_room.png">

The picture shows the main room where the whole game will take place. Note that it's game over once a piece gets stuck 
at the top of the room, so try to avoid this.

I'm only a junior programmer, so I've probably made a lot of mistakes in the code, but I'm sure you'll enjoy my code. Enjoy the game and here is my license:
## License
```
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004
 
Copyright (C) 2024 Yaroslav Nimets <yarik.nimetz@gmail.com>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.
 
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO
```